//
//  TableViewCell.swift
//  BestFlights
//
//  Created by Boocha on 10/04/2019.
//  Copyright © 2019 JiriB. All rights reserved.
//

import UIKit

class FlightCell: UITableViewCell {
    
    @IBOutlet weak var cellImageView: UIImageView!
    
    @IBOutlet weak var cityTo: UILabel!
    
    @IBOutlet weak var locationFrom: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    
    override func awakeFromNib() {
        cellImageView.layer.cornerRadius = 15
        cellImageView.clipsToBounds = true
        priceLabel.layer.cornerRadius = 10
        priceLabel.clipsToBounds = true
        super.awakeFromNib()
    }
}
