//
//  CustomImageView.swift
//  BestFlights
//
//  Created by Boocha on 10/04/2019.
//  Copyright © 2019 JiriB. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView{
    
    func loadImageFromUrl(urlString: String, cache: NSCache<NSString , UIImage>){
        guard let url = URL(string: urlString) else {return}
        
        self.image = nil
        
        if let cachedImage = cache.object(forKey: urlString as NSString){
            self.image = cachedImage
            self.applyGradient()
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            if let error = error{
                print("Error downloading the image: ", error)
            }
            
            guard let data = data else {return}
            
            DispatchQueue.main.async {
                if let downloadedImage = UIImage(data: data){
                    self.image = downloadedImage
                    self.applyGradient()
                    cache.setObject(downloadedImage, forKey: urlString as NSString)
                }
            }
        }.resume()
    }
    
    func applyGradient(){
        if let image = self.image{
            UIGraphicsBeginImageContext(image.size)
            let context = UIGraphicsGetCurrentContext()
            
            image.draw(at: CGPoint(x: 0, y: 0))
            
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            let locations:[CGFloat] = [0.0, 1.0]
            
            let top = UIColor(red: 0, green: 0, blue: 0, alpha: 1).cgColor
            let bottom = UIColor(red: 0, green: 0, blue: 0, alpha: 0).cgColor
            
            let colors = [top, bottom] as CFArray
            
            let gradient = CGGradient(colorsSpace: colorSpace, colors: colors, locations: locations)
            
            let startPoint = CGPoint(x: image.size.width/2, y: 0)
            let endPoint = CGPoint(x: image.size.width/2, y: image.size.height)
            
            context!.drawLinearGradient(gradient!, start: startPoint, end: endPoint, options: CGGradientDrawingOptions(rawValue: UInt32(0)))
            self.image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
    }
}
