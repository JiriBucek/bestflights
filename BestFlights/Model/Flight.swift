//
//  File.swift
//  BestFlights
//
//  Created by Boocha on 10/04/2019.
//  Copyright © 2019 JiriB. All rights reserved.
//

import Foundation

class Flight{
    var cityTo: String?
    var countryTo: String?
    var cityFrom: String?
    var countryFrom: String?
    var price: Int?
    var imageUrl: String?
    var timeString: String?
}
