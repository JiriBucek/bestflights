//
//  Downloader.swift
//  BestFlights
//
//  Created by Boocha on 10/04/2019.
//  Copyright © 2019 JiriB. All rights reserved.
//

import Foundation
import UIKit

class DataSource: NSObject{
    typealias CompletionHandler = (_ success: Bool) -> Void
    
    private var flights: [Flight] = []
    
    private var url: String {
       return "https://api.skypicker.com/flights?v=2&sort=popularity&asc=0&locale=en&daysInDestinationFrom=&daysInDestinationTo=&affilid=&children=0&infants=0&flyFrom=49.2-16.61-250km&to=anywhere&featureName=aggregateResults&dateFrom=\(currentDateString().today)&dateTo=\(currentDateString().afterTomorrow)&typeFlight=oneway&returnFrom=&returnTo=&one_per_date=0&oneforcity=1&wait_for_refresh=0&adults=1&limit=5"
    }
    
    let imageCache = NSCache<NSString, UIImage>()
    
    
    // Public
    func requestFlightData(completionHandler: @escaping CompletionHandler){
        guard let url = URL(string: url) else{
            completionHandler(false)
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error{
                print("Failed to request data: ", error)
                completionHandler(false)
                return
            }
            
            guard let data = data else{
                completionHandler(false)
                return
            }
            
            if let jsonObject = try? JSONSerialization.jsonObject(with: data, options: [] ) as? [String : Any], let flightsData = jsonObject["data"] as? [[String:Any]]{
                
                self.flights = []
                
                for i in 0..<flightsData.count{
                    let flight = Flight()
                    
                    if let cityFrom = flightsData[i]["cityFrom"] as? String{
                        flight.cityFrom = cityFrom
                    }
                    if let cityTo = flightsData[i]["cityTo"] as? String{
                        flight.cityTo = cityTo
                    }
                    if let price = flightsData[i]["price"] as? Int{
                        flight.price = price
                    }
                    if let countryFrom = flightsData[i]["countryFrom"] as? [String:Any], let countryName = countryFrom["name"] as? String{
                        flight.countryFrom = countryName
                    }
                    if let countryTo = flightsData[i]["countryTo"] as? [String:Any], let countryName = countryTo["name"] as? String{
                        flight.countryTo = countryName
                    }
                    if let destinationId = flightsData[i]["mapIdto"] as? String{
                        flight.imageUrl = "https://images.kiwi.com/photos/600/\(destinationId).jpg"
                    }
                    if let timeDouble = flightsData[i]["dTime"] as? Double{
                        flight.timeString = self.stringFromDateNumber(dateNumber: timeDouble)
                    }
                    self.flights.append(flight)
                }
                    completionHandler(true)
            }else{
                completionHandler(false)
                return
            }
        }.resume()
    }
    
    // Private
    private func flightForIndexPath(indexPath: IndexPath) -> Flight?{
       let row = indexPath.row
        if flights.count > row{
            return flights[row]
        }else{
            return nil
        }
    }
    
    private func stringFromDateNumber(dateNumber: Double) -> String{
        let date = Date(timeIntervalSince1970: dateNumber)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm a 'on' MMMM d, yyyy"
        return dateFormatter.string(from: date)
    }
    
    private func currentDateString() -> (today: String, afterTomorrow: String){
        let today = Date()
        let afterTomorrow = Calendar.current.date(byAdding: .day, value: 2, to: today)!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return (dateFormatter.string(from: today), dateFormatter.string(from: afterTomorrow))
    }
}

extension DataSource: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return flights.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FlightCell", for: indexPath) as? FlightCell
        
        if let flight = flightForIndexPath(indexPath: indexPath), let cityTo = flight.cityTo, let countryTo = flight.countryTo, let cityFrom = flight.cityFrom, let countryFrom = flight.countryFrom, let price = flight.price, let imageUrl = flight.imageUrl{
            
            cell?.cityTo.text = "\(cityTo), \(countryTo)"
            cell?.locationFrom.text = ("From: \(cityFrom), \(countryFrom)")
            cell?.priceLabel.text = " \(price) € "
            cell?.cellImageView.loadImageFromUrl(urlString: imageUrl, cache: imageCache)
            cell?.timeLabel.text = flight.timeString
            
        }
        return cell!
    }
}
