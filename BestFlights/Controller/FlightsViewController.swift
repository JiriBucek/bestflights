//
//  ViewController.swift
//  BestFlights
//
//  Created by Boocha on 10/04/2019.
//  Copyright © 2019 JiriB. All rights reserved.
//

import UIKit

class FlightsViewController: UIViewController {
    
    @IBAction func reloadButton(_ sender: Any) {
        loadFlights()
        flightsTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
    
    @IBOutlet weak var flightsTableView: UITableView!
        
    private var spinnerView = UIActivityIndicatorView()
    private let dataSource =  DataSource()
    private let webReachability = WebReachability()
    
    override func viewDidLoad() {
        flightsTableView.dataSource = dataSource
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadFlights()
    }
    
    private func loadFlights(){
        if webReachability.isConnectedToNetwork(){
            toggleSpinner(shouldSpin: true)
            flightsTableView.isHidden = true
            dataSource.requestFlightData(completionHandler: { success in
                DispatchQueue.main.async{
                    if success{
                        self.toggleSpinner(shouldSpin: false)
                        self.flightsTableView.reloadData()
                        self.flightsTableView.isHidden = false
                    }else{
                        self.displayMessage(userMessage: "Error while loading your data. Please try again later.")
                    }
                }
            })
        }else{
            displayMessage(userMessage: "Not connected to the internet. Please connect to the internet and reload.")
        }
    }
    
    private func toggleSpinner(shouldSpin: Bool) {
        if !shouldSpin{
            spinnerView.stopAnimating()
            return
        }
        spinnerView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        spinnerView.style = UIActivityIndicatorView.Style.whiteLarge
        spinnerView.layer.cornerRadius = 10
        spinnerView.clipsToBounds = true
        spinnerView.alpha = 0.6
        spinnerView.center = self.view.center
        spinnerView.backgroundColor = .black
        spinnerView.hidesWhenStopped = true
        self.view.addSubview(spinnerView)
        spinnerView.startAnimating()
    }
    
    private func displayMessage(userMessage: String) -> Void {
        self.flightsTableView.isHidden = true
        self.toggleSpinner(shouldSpin: false)
        let alertController = UIAlertController(title: "Error", message: userMessage, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { _ in
                    alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion:nil)
    }
}



